<?php include "header.php";
    require "config.php";
?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
       <!-- Content Header (Page header) -->
       <div class="content-header">
         <div class="container-fluid">
           <div class="row mb-2">
             <div class="col-sm-6">
               <h1 class="m-0 text-dark">Ajouter un étudiant</h1>
             </div><!-- /.col -->
             <div class="col-sm-6">
               <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="index.html">Accueil</a></li>
                 <li class="breadcrumb-item active">Tableau de bord</li>
               </ol>
             </div><!-- /.col -->
           </div><!-- /.row -->
           <div class="row">
             <a href="ajout-etudiant.php"><button type="button" class="btn btn-success">Ajouter un étudiant</button></a>
           </div><!-- /.row -->
         </div><!-- /.container-fluid -->
       </div>
       <!-- /.content-header -->
   
       <?php
   
         
         $nom    = isset($_POST['nom'])  && !empty($_POST['nom'])  ? $_POST['nom']  : '';
         $prenom = isset($_POST['prenom']) && !empty($_POST['prenom']) ? $_POST['prenom']: '';
          $tel  = isset($_POST['tel'])  && !empty($_POST['tel']) ? $_POST['tel']  : '';
         $mail = isset($_POST['mail']) && !empty($_POST['mail']) ? $_POST['mail'] : '';
     
         $submit= isset($_POST['submit'])&& !empty($_POST['submit'])? $_POST['submit'] : '';
     
         if ((isset($_POST['submit']))) {
             try {
                 $request = $pdo->prepare("INSERT INTO `etudiant`( `nom`, `prenom`, `tel`, `mail`) VALUES (:nom,:prenom,:tel,:mail)");
         
                 $request->execute([
                     'nom'=>$nom,
                     'prenom'=>$prenom,
                     'tel'=>$tel,
                     'mail'=>$mail
                     ]);
                     header('Location:etudiant.php');
     
             }
             catch (PDOException $e) {
                 echo 'Error: '.$e->getMessage();
             }
           
         }else {
             echo '<p>Veuillez remplir tous les champs SVP</p>';
         }
                          
       ?>
       
   
       <!-- Main content -->
       <section class="content">
         <div class="container-fluid">
           <!-- Small boxes (Stat box) -->
           <div class="row">
           <form  enctype="multipart/form-data" action="" method="post">
                <p>Nom</p><input type="text" name="nom" id=""><br>
                 <p>Prénom</p><input type="text" name="prenom" id=""><br>
                <p>Téléphone</p><input type="text" name="tel" id=""><br>
                <p>Mail</p><input type="email" name="mail" id=""><br>
                <input type="submit" value="valider" name='submit'>
                                  </form>
           </div>
           <!-- /.row -->
         </div><!-- /.container-fluid -->
       </section>
       <!-- /.content -->
     </div>
     <!-- /.content-wrapper -->
  
   
   
   <!-- Main content -->
   <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row"> <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
           <div id="formulaire">
                                  
                      </div>

                     
                     
                      </div>
                      <div class="icon">
                        <i class="fas fa-user-plus"></i>
                      </div>
                      <a href="#" class="small-box-footer">Modifier <i class="fas fa-arrow-circle-right"></i></a>
                      
                    </div>
                  </div>';
            }
          ?>
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
        <?php require 'footer.php' ?> 

    